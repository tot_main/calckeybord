//
//  AppDelegate.h
//  calkKeyBord
//
//  Created by Totsuka Takumi on 2014/09/27.
//  Copyright (c) 2014年 Totsuka Takumi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

