//
//  main.m
//  calkKeyBord
//
//  Created by Totsuka Takumi on 2014/09/27.
//  Copyright (c) 2014年 Totsuka Takumi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
