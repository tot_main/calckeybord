//
//  KeyboardViewController.m
//  calkKeyBord
//
//  Created by Totsuka Takumi on 2014/09/26.
//  Copyright (c) 2014年 Totsuka Takumi. All rights reserved.
//

#import "KeyboardViewController.h"

@interface KeyboardViewController ()
@property (nonatomic, strong) UIButton *nextKeyboardButton;
@property (nonatomic, strong) UILabel * calcResultLabel;
@property (nonatomic, assign) double calcNumber;
@property (nonatomic, strong) NSString * calcString;
@property (nonatomic, assign) NSInteger calcType;
@property (nonatomic, assign) BOOL decimalFlag;
@end

@implementation KeyboardViewController

- (void)updateViewConstraints {
	[super updateViewConstraints];
	
	// Add custom view sizing constraints here
	
}

- (void)viewDidLoad {
	[super viewDidLoad];
	_calcNumber = 0;
	_calcString = @"0";
	_decimalFlag = NO;
	// Perform custom UI setup here
	self.nextKeyboardButton = [UIButton buttonWithType:UIButtonTypeSystem];
	
	[self.nextKeyboardButton setTitle:NSLocalizedString(@"Next Keyboard", @"Title for 'Next Keyboard' button") forState:UIControlStateNormal];
	[self.nextKeyboardButton sizeToFit];
	self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = NO;
	
	[self.nextKeyboardButton addTarget:self action:@selector(advanceToNextInputMode) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:self.nextKeyboardButton];
	
	NSLayoutConstraint *nextKeyboardButtonLeftSideConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton
																							attribute:NSLayoutAttributeLeft
																							relatedBy:NSLayoutRelationEqual
																							   toItem:self.view
																							attribute:NSLayoutAttributeLeft
																						   multiplier:1.0
																							 constant:0.0];
	NSLayoutConstraint *nextKeyboardButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
	[self.view addConstraints:@[nextKeyboardButtonLeftSideConstraint, nextKeyboardButtonBottomConstraint]];
	
	
	
	
	// CalcResult用のLabelを配置
	_calcResultLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
	//_calcResultLabel = [[UILabel alloc]init];
	_calcResultLabel.text = @"0";
	[_calcResultLabel sizeToFit];
	_calcResultLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self.view addSubview:_calcResultLabel];
	
	[_calcResultLabel setTextAlignment:NSTextAlignmentRight];
	[_calcResultLabel setBackgroundColor:[UIColor whiteColor]];
	
	// 上
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:_calcResultLabel attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:20.0]];
	// 左
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:_calcResultLabel attribute:NSLayoutAttributeLeftMargin relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:20.0]];
	// 右
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_calcResultLabel attribute:NSLayoutAttributeRightMargin multiplier:1.0 constant:20.0]];
	
	
	// make KeyBords
	[self makeAtKeyBords];
	
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated
}

- (void)textWillChange:(id<UITextInput>)textInput {
	// The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {
	// The app has just changed the document's contents, the document context has been updated.
	
	UIColor *textColor = nil;
	if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark) {
		textColor = [UIColor whiteColor];
	} else {
		textColor = [UIColor blackColor];
	}
	[self.nextKeyboardButton setTitleColor:textColor forState:UIControlStateNormal];
}

#pragma mark - キーボードの表示 -
-(void)makeAtKeyBords {
	
	NSArray * buttonName = @[ @[ @"0", @"00", @".", @"+", @"=" ],
							  @[ @"1", @"2" , @"3", @"-", @"+/-"],
							  @[ @"4", @"5" , @"6", @"*", @"C" ],
							  @[ @"7", @"8" , @"9", @"/", @"AC"] ];
	
	// 縦に4列
	UIButton * lastVButton = self.nextKeyboardButton;
	UIButton * lastHButton = nil;
	for (int i = 0; i < 4; i++) {
		
		for (int j = 0; j < 5; j++) {
			
			UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
			
			NSString * buttonTitle = [[buttonName objectAtIndex:i]objectAtIndex:j];
			[button setTitle:buttonTitle forState:UIControlStateNormal];
			button.tag = i * 10 + j;
			
			[button sizeToFit];
			[button setBackgroundColor:[UIColor darkGrayColor] ];
			[button addTarget:self action:@selector(tapButtonItem:) forControlEvents:UIControlEventTouchUpInside];
			
			button.translatesAutoresizingMaskIntoConstraints = NO;
			[self.view addSubview:button];
			
			
			[self.view addConstraint:[NSLayoutConstraint constraintWithItem:button
																  attribute:NSLayoutAttributeWidth
																  relatedBy:NSLayoutRelationEqual
																	 toItem:nil
																  attribute:NSLayoutAttributeWidth																	 multiplier:1.0
																   constant:40.0]];
			[self.view addConstraint:[NSLayoutConstraint constraintWithItem:button
																  attribute:NSLayoutAttributeHeight
																  relatedBy:NSLayoutRelationEqual
																	 toItem:nil
																  attribute:NSLayoutAttributeHeight																	 multiplier:1.0
																   constant:30.0]];
			
			if (j == 0) {
				[self.view addConstraint:[NSLayoutConstraint constraintWithItem:button
														  attribute:NSLayoutAttributeLeft
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeLeft
														 multiplier:1.0
														   constant:10.0]];
			}else{
				[self.view addConstraint:[NSLayoutConstraint constraintWithItem:button
																	  attribute:NSLayoutAttributeLeftMargin
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:lastHButton
																	  attribute:NSLayoutAttributeRight																	 multiplier:1.0
																	   constant:15.0]];
			}
			
			[self.view addConstraint:[NSLayoutConstraint constraintWithItem:lastVButton
																  attribute:NSLayoutAttributeBottomMargin
																  relatedBy:NSLayoutRelationEqual
																	 toItem:button
																  attribute:NSLayoutAttributeBottom
																 multiplier:1.0
																   constant:30.0]];
			lastHButton = button;
			if (j==4) {
				lastVButton = button;
			}
		}
		
		lastHButton=nil;
	}
	
	
	// 送信用ボタン群
	UIButton * sendBuuton = [UIButton buttonWithType:UIButtonTypeCustom];
	[sendBuuton setTitle:@"出力" forState:UIControlStateNormal];
	[sendBuuton setBackgroundColor:[UIColor darkGrayColor] ];
	[sendBuuton sizeToFit];
	[self.view addSubview:sendBuuton];
	sendBuuton.translatesAutoresizingMaskIntoConstraints = NO;
	[sendBuuton addTarget:self action:@selector(sendButtonDidTaped:) forControlEvents:UIControlEventTouchUpInside];
	
	
	// 位置の指定
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:sendBuuton
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:nil
														  attribute:NSLayoutAttributeWidth
														 multiplier:1.0
														   constant:60.0]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:sendBuuton
														  attribute:NSLayoutAttributeHeight
														  relatedBy:NSLayoutRelationEqual
															 toItem:nil
														  attribute:NSLayoutAttributeHeight
														 multiplier:1.0
														   constant:30.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
													  attribute:NSLayoutAttributeRight
													  relatedBy:NSLayoutRelationEqual
														 toItem:sendBuuton
													  attribute:NSLayoutAttributeRight
													 multiplier:1.0
													   constant:10.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
														  attribute:NSLayoutAttributeBottom
														  relatedBy:NSLayoutRelationEqual
															 toItem:sendBuuton
														  attribute:NSLayoutAttributeBottom
														 multiplier:1.0 constant:10.0]];
	
	
	UIButton * send2byteBuuton = [UIButton buttonWithType:UIButtonTypeCustom];
	[send2byteBuuton setTitle:@"日本語" forState:UIControlStateNormal];
	[send2byteBuuton setBackgroundColor:[UIColor darkGrayColor] ];
	[send2byteBuuton sizeToFit];
	[self.view addSubview:send2byteBuuton];
	send2byteBuuton.translatesAutoresizingMaskIntoConstraints = NO;
	[send2byteBuuton addTarget:self action:@selector(send2ByteStringButtonDidTaped:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:send2byteBuuton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth  multiplier:1.0 constant:60.0]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:send2byteBuuton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:30.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:send2byteBuuton attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:sendBuuton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:send2byteBuuton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.0]];
	
	
	UIButton * send1byteBuuton = [UIButton buttonWithType:UIButtonTypeCustom];
	[send1byteBuuton setTitle:@"英字" forState:UIControlStateNormal];
	[send1byteBuuton setBackgroundColor:[UIColor darkGrayColor] ];
	[send1byteBuuton sizeToFit];
	[self.view addSubview:send1byteBuuton];
	send1byteBuuton.translatesAutoresizingMaskIntoConstraints = NO;
	[send1byteBuuton addTarget:self action:@selector(send1ByteStringButtonDidTaped:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:send1byteBuuton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth  multiplier:1.0 constant:60.0]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:send1byteBuuton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:30.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:send1byteBuuton attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:send2byteBuuton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:send1byteBuuton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.0]];
	
	
	UIButton * delBuuton = [UIButton buttonWithType:UIButtonTypeCustom];
	[delBuuton setTitle:@"削除" forState:UIControlStateNormal];
	[delBuuton setBackgroundColor:[UIColor darkGrayColor] ];
	[delBuuton sizeToFit];
	[self.view addSubview:delBuuton];
	delBuuton.translatesAutoresizingMaskIntoConstraints = NO;
	[delBuuton addTarget:self action:@selector(sendDelButtonDidTaped:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:delBuuton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth  multiplier:1.0 constant:60.0]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:delBuuton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:30.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:delBuuton attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:send1byteBuuton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:delBuuton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.0]];
	
	
	
	UIButton * enterBuuton = [UIButton buttonWithType:UIButtonTypeCustom];
	[enterBuuton setTitle:@"改行" forState:UIControlStateNormal];
	[enterBuuton setBackgroundColor:[UIColor darkGrayColor] ];
	[enterBuuton sizeToFit];
	[self.view addSubview:enterBuuton];
	enterBuuton.translatesAutoresizingMaskIntoConstraints = NO;
	[enterBuuton addTarget:self action:@selector(sendEnterButtonDidTaped:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:enterBuuton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth  multiplier:1.0 constant:60.0]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:enterBuuton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:30.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:enterBuuton attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:delBuuton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:enterBuuton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.0]];
	
	
}

-(void)tapButtonItem:(UIButton*)tapBt {
	NSLog(@"Tap at %zd", tapBt.tag);
	switch (tapBt.tag) {
		case 0:
			if (_calcString.doubleValue != 0 || _decimalFlag) {
				_calcString = [_calcString stringByAppendingString:@"0"];
			}
			break;
		case 1:
			if (_calcString.doubleValue != 0 || _decimalFlag) {
				_calcString = [_calcString stringByAppendingString:@"00"];
			}
			break;
		case 2:
			_decimalFlag = YES;
			_calcString = [_calcString stringByAppendingString:@"."];
			break;
		case 3: // +
			[self calcResult:1];
			return;
		case 4: // =
			[self calcResult:0];
			return;
		case 10:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"1"];
			break;
		case 11:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"2"];
			break;
		case 12:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"3"];
			break;
		case 13: // -
			[self calcResult:2];
			return;
		case 14: // +/-
			
			
			break;
		case 20:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"4"];
			break;
		case 21:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"5"];
			break;
		case 22:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"6"];
			break;
		case 23: // *
			[self calcResult:3];
			return;
		case 24: // C
			_calcString = @"0";
			break;
		case 30:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"7"];
			break;
		case 31:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"8"];
			break;
		case 32:
			if (_calcString.doubleValue == 0 && !_decimalFlag) {
				_calcString = @"";
			}
			_calcString = [_calcString stringByAppendingString:@"9"];
			break;
		case 33: // /
			[self calcResult:4];
			return;
		case 34: // AC
			_calcString = @"0";
			_calcNumber = 0;
			_calcType = 0;
			break;
		default:
			break;
	}
	
	// Update Label test
	[_calcResultLabel setText:_calcString];
	
}

-(void)calcResult:(NSInteger)type {
	// type = 1:+ 2:- 3:* 4:/
	double nowValue = _calcString.doubleValue;
	double result = 0;
	_decimalFlag = NO;
	// 前回の計算タイプに合わせて今入力したアイテムを計算する
	switch (_calcType) {
		case 0:
			result = nowValue;
		case 1:
			// +
			result = _calcNumber + nowValue;
			break;
		case 2:
			result = _calcNumber - nowValue;
			break;
		case 3:
			result = _calcNumber * nowValue;
			break;
		case 4:
			if (nowValue == 0 || _calcNumber == 0) {
				result = 0;
			}else{
				result = _calcNumber / nowValue;
			}
			break;
		default:
			break;
	}
	
	NSLog(@"type %zd", type);
	_calcNumber = result;
	
	// 小数点の有無をチェックする
	if (ceil(_calcNumber) == _calcNumber) {
		// 整数値
		[_calcResultLabel setText:[NSString stringWithFormat:@"%.0f", _calcNumber]];
	}else{
		// 小数点
		[_calcResultLabel setText:[NSString stringWithFormat:@"%.14f", _calcNumber]];
	}
	
	_calcString = @"0";
	_calcType = type;
}

-(void)sendButtonDidTaped:(UIButton*)tapBt {
	NSString * sendText = [_calcResultLabel.text stringByAppendingString:@"\n"];
	[self.textDocumentProxy insertText:sendText];
}

-(void)send2ByteStringButtonDidTaped:(UIButton*)tapBt
{
	double sendCount = _calcResultLabel.text.doubleValue;
	if (sendCount > 10000) {
		[self.textDocumentProxy insertText:@"上限は一万文字です"];
	}
	
	NSString * endString = @"おわり";
	
	NSMutableString * sendText = [[NSMutableString alloc]init];
	NSInteger i = 0, j = (NSInteger)sendCount;
	for (i=1; i <= j - endString.length; i++) {
		if (i%1000 == 0) {
			[sendText appendString:@"千"];
		}else if( i%100 == 0){
			[sendText appendString:@"百"];
		}else if(i%10 == 0){
			[sendText appendString:@"十"];
		}else{
			[sendText appendString:@"あ"];
		}
	}
	[self.textDocumentProxy insertText:sendText];
	[self.textDocumentProxy insertText:endString];
}

-(void)send1ByteStringButtonDidTaped:(UIButton*)tapBt
{
	double sendCount = _calcResultLabel.text.doubleValue;
	if (sendCount > 10000) {
		[self.textDocumentProxy insertText:@"上限は一万文字です"];
	}
	
	NSString * endString = @"END";
	
	NSMutableString * sendText = [[NSMutableString alloc]init];
	NSInteger i = 0, j = (NSInteger)sendCount;
	for (i=1; i <= j - endString.length; i++) {
		if (i%1000 == 0) {
			[sendText appendString:@"T"];
		}else if( i%100 == 0){
			[sendText appendString:@"H"];
		}else if(i%10 == 0){
			[sendText appendString:@"O"];
		}else{
			[sendText appendString:@"o"];
		}
	}
	[self.textDocumentProxy insertText:sendText];
	[self.textDocumentProxy insertText:endString];
}

-(void)sendDelButtonDidTaped:(UIButton*)tapBt {
	[self.textDocumentProxy deleteBackward];
}

-(void)sendEnterButtonDidTaped:(UIButton*)tapBt {
	[self.textDocumentProxy insertText:@"\n"];
}

@end
